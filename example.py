import xplane_udp_connector.xplane_udp_connector as xpc
import pid_controller.pid_controller as pid

# class TimedAutomata:
#
#     def __init__(self, variables, clocks):
#         self._current_state = None
#         self._states = dict()
#         self._transitions = dict()
#         self._invariants = dict()
#         self.variables = variables
#         self.clocks = clocks
#
#     def add_state(self, state):
#         self._states[state.name] = state
#         self._invariants[state.name] = state.invariant
#
#     def add_transition(self, state_a_name, state_b_name, guard_predicate):
#         self._transitions[state_a_name] = (state_b_name, guard_predicate)
#
#     def run(self, external_data):
#         fulfill = self.check_invariant()
#         self._current_state.run(self.variables, self.clocks)
#         self.check_transition()
#         self.update_clocks()
#
#     def check_transition(self):
#         target_state, predicate = self._transitions[self._current_state.name]
#         if predicate(self.variables, self.clocks):
#             self.make_transition(target_state)
#
#     def make_transition(self, target_state):
#         self._current_state = self._states[target_state]
#         self._current_state.init(self.variables, self.clocks)
#
#     def check_invariant(self):
#         invariant = self._invariants[self._current_state.name]
#         return invariant(self.variables, self.clocks)
#
#
#     def update_clocks(self):
#         self.clocks = {name: value+1 for (name, value) in self.clocks.items()}
#
#     @staticmethod
#     def update_clock(clock):
#         # (name, value, flow)
#         name, value, flow = clock
#         return name, value + flow, flow
#
# class AutomataState:
#
#     def __init__(self, name):
#         self.name = name
#
#     def run(self, variables, clocks):
#         pass
#
#     def update(self, variables, clocks):
#         pass
#
#     def invariant(self, variables, clocks):
#         return True
#
#
# class StandState(AutomataState):
#
#     def run(self, variables, clocks):
#         pass
#
#     def update(self, variables, clocks):
#         variables["throttle"] = 0.3
#
#     def invariant(self, variables, clocks):
#         # Wait 1 second before next state
#         return clocks["x"] < 1000
#
# class RunState(AutomataState):
#
#     def run(self, variables, clocks):
#         pass
#
#     def update(self, variables, clocks):
#         # restart clock
#         clocks["x"] = 0
#         variables["throttle"] = 1.0
#
#     def invariant(self, variables, clocks):
#         # Wait 5 second
#         return clocks["x"] < 5000
#
# class TakeoffState(AutomataState):
#
#     def run(self, variables, clocks):
#         pass
#
# class StraightLine(AutomataState):
#
#     def run(self, variables, clocks):
#         delta = pidc.output(80.0, vpath)
#
# variables = {'throttle': 0.}
# clocks = {'x': 0}
# automata = TimedAutomata(variables, clocks)
# automata.add_state(StandState('stand'))
# automata.add_state(RunState('run'))
# automata.add_state(TakeoffState('takeoff'))
# automata.add_transition('stand', 'run', lambda variables, clocks : clocks['x'] >= 1000)


s_vpath = 0
s_alt = -1
pidc = pid.PIDController(0.09, 0.3, 0.0, -1.0, 1.0)
pidc2 = pid.PIDController(0.09, 0.3, 0.0, -100, 100)

def receive(data):
    global s_vpath
    global s_alt
    global pidc
    global pidc2

    vpath = data[18][2]
    if not s_vpath:
        s_vpath = vpath



    mph = data[3][0]
    alt = data[20][3]

    if s_alt == -1:
        s_alt = alt

    # print('speed:', mph, 'alt:', alt)

    takeoff = 1 if mph > 70 else 0

    beta = data[18][1]
    delta2 = pidc2.output(100, alt)
    elev = takeoff * delta2 * 0.002
    print('delta2', delta2)

    delta = pidc.output(80.0, vpath)

    ailrn = takeoff * delta
    # print('delta', delta, ', path:', s_vpath)

    # Send new nosewheel and aileron angles
    return [(8, elev, ailrn, delta, 0.0, 0.0, 0.0, 0.0, 0.0)]



# XPlane connector initialization
xplane_connector = xpc.XPlaneUDPConnector()

# Set receive / send node function
xplane_connector.receive = receive

# Start receiving and sending data
xplane_connector.start()
xplane_connector.join()
