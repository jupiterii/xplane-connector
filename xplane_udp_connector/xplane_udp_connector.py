import socket
import struct
from threading import Thread


class XPlaneUDPConnector(Thread):
    """This class help to make a connection between XPlane and a python program"""

    # XPlane software ip host
    UDP_IP = "127.0.0.1"
    # XPlane software configured receive port
    UDP_PORT = 49003
    # XPlane software configured sending port
    UDP_SEND_PORT = 49000
    # The node function for receiving from and sending data to XPlane
    receive = None

    def __init__(self):
        """Initialize the connection to xplane"""
        Thread.__init__(self)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((self.UDP_IP, self.UDP_PORT))

    def close(self):
        """Close the udp socket"""
        self.sock.close()

    def run(self):
        """start receiving and sending data to XPlane"""

        while True:

            # Receive data from XPlane
            data, addr = self.sock.recvfrom(1024)
            data_raw = data[5:]
            length = len(data_raw)
            nb = length // 36

            # Decode data
            data_decoded = [struct.unpack('iffffffff', data_raw[n * 36:n*36+36]) for n in range(nb)]

            # Fill a hash table with data header as key and values as value
            hash_data = {d[0]: d[1:] for d in data_decoded}

            # If function receive is defined send the data returned by this function
            if self.receive:
                self.send(self.receive(hash_data))

    def send(self, data):
        """Send data to XPlane"""

        # Create a buffer
        byte_number = 36 * 2
        buf = bytearray(5 + byte_number)
        # Set header to buffer
        header = (68, 65, 84, 65, 0)
        struct.pack_into('bbbbb', buf, 0, *header)

        # Pack data to buffer
        for i, d in enumerate(data):
            struct.pack_into('iffffffff', buf, 5 + i * 36, *d)

        # Send buffer to XPlane
        self.sock.sendto(buf, (self.UDP_IP, self.UDP_SEND_PORT))