import xplane_udp_connector.xplane_udp_connector as xpc
import pid_controller.pid_controller as pid
import time
import itertools

class TimedAutomata:

    def __init__(self, variables, clocks):
        self._current_state = None
        self._states = dict()
        self._transitions = dict()
        self.data = dict()
        self.variables = variables
        self.clocks = clocks
        self._time = 0.

    def add_state(self, state):
        self._states[state.name] = state

    def add_transition(self, state_a_name, state_b_name, guard_predicate):
        self._transitions[state_a_name] = (state_b_name, guard_predicate)

    def define_init_state(self, state_name):
        self._current_state = self._states[state_name]
        self._current_state.init(self.variables, self.clocks)

    def init(self):
        self._time = time.time()

    def run(self, data):
        self.data = data
        fulfill = self.check_invariant()
        output_values = self._current_state.run(self.variables, self.clocks, self.data)
        self.check_transition()
        self.update_clocks()
        return output_values

    def check_transition(self):
        if self._current_state.name not in self._transitions:
            return

        target_state, predicate = self._transitions[self._current_state.name]
        if predicate(self.variables, self.clocks):
            self.make_transition(target_state)

    def make_transition(self, target_state):
        self._current_state = self._states[target_state]
        self._current_state.init(self.variables, self.clocks)
        print('make a transition to state : ', target_state)

    def check_invariant(self):
        invariant = self._states[self._current_state.name].invariant
        return invariant(self.variables, self.clocks)


    def update_clocks(self):
        now = time.time()
        delta_time = now - self._time
        self._time = now
        self.clocks = {name: value + delta_time for (name, value) in self.clocks.items()}

    @staticmethod
    def update_clock(clock):
        # (name, value, flow)
        name, value, flow = clock
        return name, value + flow, flow

class AutomataState:

    def __init__(self, name):
        self.name = name

    def run(self, variables, clocks, data):
        pass

    def init(self, variables, clocks):
        pass

    def invariant(self, variables, clocks):
        return True


class StandState(AutomataState):

    def run(self, variables, clocks, data):
        # data['start_hpath'] = data['']
        return [(25, variables["throttle"], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)]

    def init(self, variables, clocks):
        variables["throttle"] = 0.4

    def invariant(self, variables, clocks):
        # Wait 1 second before next state
        return clocks["x"] < 10.

class RunState(AutomataState):

    def run(self, variables, clocks, data):
        return [(25, variables["throttle"], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)]

    def init(self, variables, clocks):
        # restart clock
        clocks["x"] = 0
        variables["throttle"] = 1.0

    def invariant(self, variables, clocks):
        # Wait 5 second
        return clocks["x"] < 10_000

class TakeoffState(AutomataState):

    def run(self, variables, clocks, data):
        pass

class StraightLine(AutomataState):

    def init(self, variables, clocks):
        # PID controller for straight line
        self.pidc = pid.PIDController(0.09, 0.6, 0.0, -1.0, 1.0)
        # Set h-path target
        self.hpath_target = 85.5 # data["start_hpath"]

    def run(self, variables, clocks, data):
        # actual h-path
        hpath = data["sensors"][18][2]
        # compute delta for reach the h-path target
        delta = self.pidc.output(self.hpath_target, hpath)
        return [(8, 0.0, 0.0, delta, 0.0, 0.0, 0.0, 0.0, 0.0)]

variables = {'throttle': 0.}
clocks = {'x': 0}

automata = TimedAutomata(variables, clocks)
automata.add_state(StandState('stand'))
automata.add_state(RunState('run'))
automata.add_state(TakeoffState('takeoff'))
automata.add_transition('stand', 'run', lambda variables, clocks : clocks['x'] >= 10.)
automata.define_init_state('stand')
automata.init()

navigation_automata = TimedAutomata(variables, clocks)
navigation_automata.add_state(StraightLine('straight-line'))
navigation_automata.define_init_state('straight-line')
navigation_automata.init()

def receive(data):

    all_data = {'sensors': data}
    output1 = automata.run(all_data)
    output2 = navigation_automata.run(all_data)

    output = list(itertools.chain(output2, output1))
    print(output)
    # mph = data[3][0]
    # alt = data[20][3]
    # print('speed:', mph, 'alt:', alt)
    # takeoff = 1 if mph > 70 else 0
    # beta = data[18][1]
    # elev = 0.0

    # delta = pidc.output(80.0, vpath)

    # Send new nosewheel and aileron angles
    # return [(11, 0, 0, delta, -999.0, -999.0, -999.0, -999.0, -999.0)]
    return output



# XPlane connector initialization
xplane_connector = xpc.XPlaneUDPConnector()

# Set receive / send node function
xplane_connector.receive = receive

# Start receiving and sending data
xplane_connector.start()
xplane_connector.join()
